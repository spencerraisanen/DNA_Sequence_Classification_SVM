This project deals with the classification of DNA Sequences using a Support
Vector Machine. The SVM has been created with no external libraries and
various kernels have been implemented in order to test the efficiency of all
kernel types. 

The data conditioning aspect involves in effect the application of a different
kernel known as the k-mers kernel. Different aspects of the k-mers kernel have been
attempted.

The overall best result for the DNA classfication arose from using the kernel
combination of the k-mers with k = 5 and with a polynomial kernel of p = 2.